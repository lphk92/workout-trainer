import yaml
from gtts import gTTS
from pathlib import Path

words = [
    "Begin",
    "Rest",
    "Next Up",
    "Workout Complete",
    "Switch Sides",
    "Continue",
    "Ten",
    "Three",
    "Two",
    "One",
]


def get_audio_filename(text):
    return text.replace(" ", "_").strip() + ".mp3"


def generate_audio(text, output_dir, *, overwrite=False):
    output_path = Path(output_dir)
    output_path.mkdir(parents=True, exist_ok=True)
    target = output_path / get_audio_filename(text)

    if not target.exists() or overwrite:
        gTTS(text=text, lang='en', slow=False).save(str(target))


def generate_workout_audio(workout_dir, output_dir, *, overwrite=False):
    for p in Path(workout_dir).iterdir():
        if not p.suffix == ".yaml":
            continue
        print(f"Generating audio for {p}")
        workout = yaml.safe_load(p.read_text())
        for exercise in workout['exercises']:
            generate_audio(exercise['name'], output_dir, overwrite=overwrite)


def load_workout(workout_file):
    workout = yaml.safe_load(Path(workout_file).read_text())

    default_exercise = {"switches": 0, "switch_rest": 3}
    default_exercise.update(workout['defaults'])
    exercises = []
    for exercise in workout['exercises']:
        new_exercise = dict(default_exercise)
        new_exercise.update(exercise)
        new_exercise["audio_file"] = get_audio_filename(exercise['name'])
        # TODO: Validate exercise structure before adding
        exercises.append(new_exercise)

    total_seconds = sum(
        e['switches'] * e['switch_rest'] + e['rest'] + e['duration']
        for e in exercises
    )
    duration = (
        f"{total_seconds}s"
        if total_seconds < 60 else
        f"{total_seconds // 60}m {total_seconds % 60}s"
    )

    return {
        'name': workout['name'],
        'exercises': exercises,
        'duration': duration,
    }


def main():
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("input_dir")
    parser.add_argument("output_dir")
    parser.add_argument("--overwrite", default=False, action="store_true")
    args = parser.parse_args()

    for word in words:
        generate_audio(word, args.output_dir)

    generate_workout_audio(args.input_dir, args.output_dir, overwrite=args.overwrite)
    print("Done")


if __name__ == "__main__":
    main()
