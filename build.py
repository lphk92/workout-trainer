#!/usr/bin/env python3
import jinja2 as j2
import os
import yaml
from pathlib import Path


template_dir = Path('templates')
template_env = j2.Environment(
    loader=j2.FileSystemLoader(str(template_dir))
)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("output_dir")
    parser.add_argument("--base-url", default="")
    args = parser.parse_args()

    import sys
    from generate import load_workout

    build_root = Path(args.output_dir)
    workout_root = Path("./workouts/")

    def render(page_name, template=None, **kwargs):
        print(f"Rendering page: {page_name}")
        kwargs["page"] = page_name
        kwargs["base_url"] = args.base_url
        if template is None:
            raw_html = template_env.get_template(f'{page_name}.j2.html').render(**kwargs)
        else:
            raw_html = template_env.get_template(template).render(**kwargs)

        target_dir = build_root / Path(f'{page_name}' if page_name != "index" else ".")
        target_dir.mkdir(parents=True, exist_ok=True)
        (target_dir / 'index.html').write_text(raw_html)

    workouts = {}
    for workout_file in workout_root.glob("*.yaml"):
        workout = load_workout(workout_file)
        render(workout_file.stem, template="workout.j2.html", workout=workout)
        workouts[workout_file.stem] = workout

    render('index', workouts=workouts)

    print("Done!")
