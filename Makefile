.RECIPEPREFIX += 

build: audio

    python3 build.py public/
    cp -r static/ public/

audio:

    python3 generate.py workouts/ static/audio/

serve: build

    python -m http.server --directory public/ 8081

clean:

    rm -rf public/
